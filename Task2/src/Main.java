public class Main {
    public static void main(String[] args) {
        // Переопределение
        Client client1 = new Client("Леха",500);
        Client client2 = new Client("Миша",1000);
        Transfer transfer = new Transfer(client1,client2,100);
        Credit credit = new Credit("Ипотека",5000,client1);
        System.out.println(client1.toString());
        System.out.println(transfer.toString());
        System.out.println(credit.toString());

        //--------------------------------------------------------------------
        credit.receivingCredit();
        credit.extinguished();
    }
}
