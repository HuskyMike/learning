import java.util.Objects;

public class Credit implements CreditService {
    private String nameCredit;
    private int summCredit;
    private Client client;
    private String nameClient;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return summCredit == credit.summCredit &&
                Objects.equals(nameCredit, credit.nameCredit) &&
                Objects.equals(client, credit.client) &&
                Objects.equals(nameClient, credit.nameClient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameCredit, summCredit, client, nameClient);
    }

    @Override
    public String toString() {
        return "{" + "\n" +
                " Credit" + "\n [\n" +
                "   nameCredit: '" + nameCredit + "',\n" +
                "   summCredit: " + summCredit + ",\n" +
                "   nameClient: " + nameClient + "\n ]\n" +
                '}';
    }

    public Credit(String nameCredit, int summCredit, Client client) {
        this.nameCredit = nameCredit;
        this.summCredit = summCredit;
        this.client = client;
        this.nameClient = client.getName();
    }


    @Override
    public void receivingCredit() {
        System.out.println("Кредит одобрен");
        client.setScore(client.getScore() + summCredit);
    }

    @Override
    public void extinguished() {
        System.out.println("Кредит погашен");
        client.setScore(client.getScore() - summCredit);
    }
}
